<?php
	// Ceci est un webservice. Il permet d'indiquer que l'utilisateur se déconnecte.

	session_start(); // On démarre une session	
	$_SESSION['connexion'] = 0; // 1 pour connecté, 0 pour déconnecté
	
 	http_response_code(200); // On prévient que tout s'est bien passé et que c'est trop la fête 	
?>
