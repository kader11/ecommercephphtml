<?php

	include ('entete.php');
	
// initialisation du compteur d'emails dans la session, si ce n'est pas déjà fait
if (! isset($_SESSION['nb_emails'])) {
	$_SESSION['nb_emails'] = 0;
}

// test : l'utilisateur a-t-il épuisé son quota de 5 emails ?
if ($_SESSION['nb_emails'] >= 5) {
	echo 'Vous avez déjà envoyé vos 5 emails !';
	header('Refresh: 2; url=contact.php');
}

// vérification du formulaire : tous ls champs sont-ils bien remplis ?
if (empty($_REQUEST['nomContact']) || empty($_REQUEST['objetContact']) || empty($_REQUEST['messageContact'])) {
	// le formulaire n'a pas été correctement rempli : redirection
	//header('Location: formulaire.html'); // redirection immédiate, ou...
	 // header('Refresh: 2; url=contact.php'); // ...redirection après 2 secondes
	echo "Le formulaire n'était pas rempli correctement... redirection dans 2 secondes";
	
}

// affichage de débogage des variables reçues :
//var_dump($_REQUEST);

// récupération des variables, de différentes façons équivalentes
$destinataire = 'nassim.bendjoudi@gmail.com, abderezak.zadri@etu.umontpellier.fr, t.bondetti@live.fr ';
$nom = $_REQUEST['nomContact'];
$objet = $_REQUEST['objetContact'];
$message = $_REQUEST['messageContact'];
//$headers = 'From: '. $_REQUEST['mailContact'];

// envoi de l'email et récupération de la valeur de retour de la fonction mail()
$ok = mail($destinataire, $objet, $message);

// test de cette valeur de retour
if ($ok) {
	echo "Le message a bien été envoyé";
} else {
	echo "Erreur lors de l'envoi du message.</br>Veuillez nous excuser et recommencer un autre jour ou contactez-nous directement à l'une des adresses mails suivantes : <ul> <li>nassim.bendjoudi@gmail.com</li><li> abderezak.zadri@etu.umontpellier.fr</li><li>t.bondetti@live.fr</li></ul> ";
}
echo "<br/>";

// un email a été envoyé - ATTENTION ce code devrait se trouver dans le if ($ok) afin de
// ne comptabiliser l'envoi que s'il a réussi, mais le serveur nous interdisant d'envoyer
// les messages, on fait le décompte même en cas d'échec afin de montrer le mécanisme
$_SESSION['nb_emails']++;

	include('pied.html');
	exit;

?>
