<?php
	// Ceci est un webservice. Il renvoie des données au format JSON

	header('Content-Type: application/json'); // Entête pour le type de contenu (JSON)

	function get_tableau(){
		include('bdd.php');
		$requete = 'SELECT * FROM releves';
		$base=connexionbd();
		$donnees = requete($base, $requete);
		return $donnees;	
	}

	http_response_code(200);

	echo json_encode(get_tableau());
?>
