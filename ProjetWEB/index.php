<?php
	include('entete.php');
	
	include('bdd.php');
	
	// CES TROIS LIGNES SONT A SUPPRIMER APRES UNE PREMIERE CONNEXION, ELLES SERVENT A LA CREATION DE LA TABLE ET DES EXEMPLE. SI ON NE SUPPRIME PAS vidage_table, a chaque fois que l'on retournera sur l'accueil, la base de données sera vidée, si on ne supprime pas insertion_exemples alors que l'on a retiré vidage_table(), les exemples seront créés à chaque fois !
	vidage_table(); 
	creation_table();
	insertion_exemples();
?>
<div id='messageBienvenue'>
	BIENVENUE SUR NZT PLANTES, LE SITE DE CONSULTATION DE NOTRE BASE DE DONNEES DE BOTANIQUE !
</div>

<section id="slideshow">
		
	<div class="container">
		<div class="c_slider"></div>
		<div class="slider">
		
			<figure>
				<img src="IMG/essai.png" alt="" width="840" height="510" />
				
			</figure><!--
		
			--><figure>
				<img src="IMG/geranium.jpg" alt="" width="840" height="510" />
				<figcaption>Geranium</figcaption>

			</figure><!--
			--><figure>
				<img src="IMG/acacia.jpg" alt="" width="840" height="510" />
				<figcaption>Acacia</figcaption>
			</figure><!--
			--><figure>
				<img src="IMG/pin.jpg" alt="" width="840" height="510" />
				<figcaption><em>(Pin)</em> d\'Alep</figcaption>

			</figure><!--
			--><figure>
				<img src="IMG/trillium.jpg" alt="" width="840" height="510" />
				<figcaption>Trillium</figcaption>
			</figure>
		</div>
	</div>
		
	<span id="timeline"></span>
</section>

<?php
	include('pied.html');
?>
