// Ceci est le fichier javascript qui rend notre application dynamique !


document.addEventListener('DOMContentLoaded', function () {
	
	function afficheConnexion(){
		if (document.getElementById('connexion').style.display == 'inline'){
			document.getElementById('connexion').style.display = 'none';
			document.getElementById('formConnexion').style.display = 'inline';			
		}
	}
	
	function authentification(){
		var login = document.getElementById('login').value;
		var mdp = document.getElementById('motDePasse').value;
		console.log(login);
		console.log(mdp);
		
		var request = new XMLHttpRequest();
		
		request.addEventListener('load', function(data){
			console.log('on entre dans la fonction'); //NE S'AFFICHE PAS POURQUOI DONC ??
			var reponse = data.target.responseText;
			if (reponse='correcte'){
				console.log('La réponse est correcte');
			}else{
				console.log('La réponse est incorrecte');
			}
		
		});
		
		request.open("GET", "../PHP/connexion.php?login=" + login + "&mot_de_passe=" + mdp);
		request.send();
	}
	
	document.getElementById('connexion').addEventListener('click',afficheConnexion);
	document.getElementById('validerConnexion').addEventListener('click', authentification);
	
});
