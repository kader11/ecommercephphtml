<?php
	// Ceci est un webservice. Il vérifie que le login et mdp sont valides.

	$login=$_GET['login'];
	$mdp=$_GET['mot_de_passe'];
	
	$reponse='incorrecte';
	
	$donneesJson = file_get_contents('../users.json'); // Récupère le contenu du fichier .json sous forme d'une chaine de caractères
	
	$liste = json_decode($donneesJson, true); // Transforme en tableau
	
	foreach($liste as $log=>$tab){
		if ($log == $login){
			if($tab['mot_de_passe'] == $mdp){
				$reponse='correcte';
			}
		}
	}
	http_response_code(200); // On prévient que tout s'est bien passé et que c'est trop la fête
	
	echo($reponse);

?>
