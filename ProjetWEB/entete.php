<!doctype html>
<?php
	session_start();
?>
<html>
	<head>
		<html lang="fr">
		
		<meta charset="UTF-8">
		<meta name="robots" content="index,follow" />
		
		<title>NZT Plantes</title>
		<link rel="stylesheet" type="text/css" href="CSS/stylepage.css">
		<link rel="stylesheet" type="text/css" href="CSS/slide.css">
		<script src="JS/connexion.js"></script>
		<script src="JS/deconnexion.js"></script>
	</head>

	<body>
	
	<div id="entete">
		<div id="logo">		
			<img src="IMG/LOGO.png"/>
		</div>
		
		</br>
		
		<h1>NZT PLANTES</h1>
		
		<form id="formConnexion" style="display:none">
			<fieldset>
				<legend>Connexion</legend>
				<label>login : </label>
				<input id="login" type="text" placeholder="votre login" required />
				<label>mot de passe :</label>
				<input id="motDePasse" type="password" placeholder="mot de passe" required />
				<button id="validerConnexion">&#10004;</button>
			</fieldset>
		</form>
		
		<div id="erreurConnexion" style="display:none">
			Login ou mot de passe incorrect.
		</div>			
	</div>

	<!-- MENU HORIZONTAL-->
	<div id="menu-horizontal">
	
		<nav>
			<ul>
				<li class="menu-home"><a href="index.php">Accueil</a></li>
				<li class="menu-plantes"><a href="plantes.php">Nos Plantes</a></li>
				<li class="menu-plantesDyn"><a href="plantesDyn.php">Effectuer une recherche</a></li>
				<li class="menu-contact"><a href="contact.php">Nous contacter</a></li>
	<?php
		$displayLogIn='inline';
		$displayLogOut='none';
		$displayAjouter='none'; // SERVIRA POUR planteDyn.php, (cela déclare cette variable pour chaque page mais permet de ne tester qu'une seule fois la condition de connexion 
	
		if (array_key_exists('connexion', $_SESSION)){
			if ($_SESSION['connexion'] == 1){ // Si on est connecté
				$displayLogIn='none';
				$displayLogOut='inline'; 
				$displayAjouter='inline';
			}
		}
		echo('
			<li class="menu-newusr" style="display:' . $displayLogIn . '"><a href="formulaireCreation.php">Créer un compte</a></li>
			<li id="connexion" class="menu-cnx" style="display:' . $displayLogIn . '">"<a href="#">Se connecter</a></li>
			<li id="deconnexion" class="menu-cnx" style="display:' . $displayLogOut . '"><a href="#">Se déconnecter</a></li>
			');
	?>
			
			</ul>
		</nav>
	
	</div>
	
	<!-- MENU DEROULANT-->
	<ul id="menu-deroulant">
		<li><a href="#">MENU</a>
			<!--<button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">MENU</button>-->
			<ul>
				<li class="menu-home2"><a tabindex="-1" href="index.php">Accueil</a></li>
				<li class="menu-plantes2"><a tabindex="-1" href="plantes.php">Nos plantes</a></li>
				<li class="menu-plantesDyn2"><a tabindex="-1" href="plantesDyn.php">Effectuer une recherche</a></li>
				<li class="menu-contact2"><a tabindex="-1" href="contact.php">Nous contacter</a></li>
				
			
	<?php
	
		echo('
			<li class="menu-newusr2" style="display:' . $displayLogIn . '"><a  tabindex="-1" href="formulaireCreation.php">Créer un compte</a></li>
			<li id="connexion2" class="menu-cnx2" style="display:' . $displayLogIn . '"><a tabindex="-1" href="#">Se connecter</a></li>
			<li id="deconnexion2" class="menu-cnx2" style="display:' . $displayLogOut . '"><a  tabindex="-1" href="#">Se déconnecter</a></li>
			');
	?>
		
			</ul>
		</li>
	</ul>
	
	<div class="corps">

	
	
