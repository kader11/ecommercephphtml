// Ceci est le fichier javascript qui permet de déconnecter du site !

document.addEventListener('DOMContentLoaded', function () {
	
	function deconnexion(){	
		var request = new XMLHttpRequest();
		
		request.addEventListener('load', function(data){
			window.location.reload();
		});
		
		request.open("GET", "deconnexion.php");
		request.send();
	}
	document.getElementById('deconnexion2').addEventListener('click', deconnexion);
	document.getElementById('deconnexion').addEventListener('click', deconnexion);
});
