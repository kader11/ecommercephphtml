// Ceci est le fichier javascript qui rend notre application dynamique !


document.addEventListener('DOMContentLoaded', function () { // Permet d'attendre que la page soit chargée pour exécuter le javascript, permet de placer ainsi ce script dans le <head> 

	function basesTableau(){ // Cette fonction renvoie le code_html (format CdC) décrivant la structure (nom des colones) du tableau que l'on affichera 
		var tab_html ='';
		tab_html += '<table>';
		tab_html += '<thead>';
		tab_html += '<tr>';
		tab_html += '<th>Photo</th>';
		tab_html += '<th>ID</th>';
		tab_html += '<th>titre/th>';
		tab_html += '<th>description</th>';
		tab_html += '<th>categorie/th>';
		tab_html += '<th>nom-vendeur</th>';
		tab_html += '<th>prix</th>';
		tab_html += '<th>rdv_lat</th>';
		tab_html += '<th>rdv_lon</th>';
		tab_html += '<th>date_ajout</th>';
		tab_html += '</tr></thead>';
		return tab_html;
	}

	function refresh(){	
		if (document.getElementById('tableauAnnonces').innerHTML == ''){ // FONCTIONNE AUSSI AVEC document.querySelector('#tableauAnnonces').innerHTML .. DEMANDER LA DIFFERENCE ENTRE querySelector('#.') ET getElementById('.'), LEQUEL EST LE PLUS ADEQUAT ?
			
			var request = new XMLHttpRequest(); // On prépare une requête
			request.open("GET", "PHP/get_tableau.php"); // On précise que la requête sera envoyée à get_tableau.php ?? EST CE CORRECTE ??
			request.send(); // On envoie la requête (a get_tableau.php)
			
			
			request.addEventListener('load', function(data){ // On définit ce que la fonction refresh() fera lorsqu'elle aura reçu une réponse de la part de get_tableau.php
			
				var tab = JSON.parse(data.target.responseText); // Converti la chaine de Caractère au format JSON résulant de la requête (retournée par get_tableau.php) en un tableau associatif (dico)!
				var tab_html = basesTableau(); // On stocke dans la variable tab_html de le code html décrivant la structure du tableau que l'on souhaite afficher
				
				for(var numero in tab){ // On parcourt le dictionnaire "tab" (voir sa structure à l'adresse suivante : http://localhost/.../ProjetWEB/PHP/get_tableau.php)
					tab_html += '<tr>'; // On va écrire un ligne de notre tableau html
					tab_html += '<td> <img class= "imagetab" src="'; // La premiere colone du tableau html contient les photos des annonces
					tab_html += tab[numero]['photo'];
					tab_html += '"/> </td>';
					for(var cle in tab[numero]){ // Chaque tab[numero] est lui-même un tableau associatif contenant les informations (id, photo, nom, etc.) de l'annonce dont l'id est égat à numero+1
						if (cle != 'photo'){
							tab_html +='<td>';
							tab_html += tab[numero][cle];
							tab_html +='</td>';
						}
					}
					tab_html += '</tr>';
				}	
			
				tab_html += '</table>';
				document.querySelector('#tableauAnnonces').innerHTML = tab_html; // On ajoute à la division dont l'id est 'tableauAnnonces' (dans liste.php) tous le code html de tab_html
				document.getElementById('afficherBDD').innerHTML = 'Masquer La Base De Données'; // On change le bouton
			});
		}else{
			document.querySelector('#tableauAnnonces').innerHTML ='';
			document.getElementById('afficherBDD').innerHTML = 'Afficher La Base De Données';
		}
		
	}
	
	document.getElementById('afficherBDD').addEventListener('click', refresh);
	
});
