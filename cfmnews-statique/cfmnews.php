<?php
	// lecture des infos existantes
	$infos = json_decode(file_get_contents('storage.json'), true);

	// y a-t-il une nouvelle info postée ?
	if (! empty($_POST['info'])) {
		// ajout aux infos existantes et sauvegarde
		array_push($infos, [
			"info" => $_POST['info'],
			"date" => date('Y-m-d H:i:s')
		]);
		file_put_contents('storage.json', json_encode($infos));
	}
?>

<html>
	<head>
		<title>Correction TD 6 ex 3</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="bootstrap.css">
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>

	<body>
		<h1>CFM News</h1>
		<p class="lead">La meilleure application d'information participative !</p>

		<div class="well" id="info-zone">
		<?php
			// affichage des infos
			foreach ($infos as $i) {
				echo '<div class="une-info"><label>'  . $i['date'] . '</label> ' . $i['info'] . '</div>';
			}
		?>
		</div>

		<form class="form-inline" method="POST">
			<div class="form-group">
				<input id="nouvelle-info" name="info" class="form-control" type="text" placeholder="Ex: remaniement ministériel">
			</div>
			<button class="btn btn-primary">Envoyer l'info</button>
		</form>	
		<a href="" class="btn btn-warning">Actualiser</a>
	</body>
</html>

