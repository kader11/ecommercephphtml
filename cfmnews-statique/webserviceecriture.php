<?php
	// lecture des infos existantes
	$infos = json_decode(file_get_contents('storage.json'), true);

	// y a-t-il une nouvelle info postée ?
	if (! empty($_POST['info'])) {
		// ajout aux infos existantes et sauvegarde
		array_push($infos, [
			"info" => $_POST['info'],
			"date" => date('Y-m-d H:i:s')
		]);
		file_put_contents('storage.json', json_encode($infos));
	}
?>
