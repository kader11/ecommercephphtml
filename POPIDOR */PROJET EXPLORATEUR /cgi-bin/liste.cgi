#!/usr/bin/env python3
import os, re, cgi


def parcours(repertoire, numero, affichage) :
	contenu = os.listdir(repertoire)
	print('<ul>')
	nom = numero;
	for element in contenu :
		if os.path.isdir(repertoire+"/"+element) :
			numero +=1
			print('<li id="idDivision{num}" name="Liste{nom}" style="display:{aff}">'.format(num=numero, aff = affichage, nom=nom))
			
			print('<img id="idImage{}" src="../img/folder.png" width="25" height="25" onClick="ouvertureFermeture(idImage{}, idDivision{}, {})" />'.format(numero, numero, numero, numero))
			print('<a  href="http://localhost/~azadri/cgi-bin/contenu.cgi?rep='+repertoire+'/'+element+'" target="corps" >', element)
			print('</a>')
			
			numero = parcours(repertoire+"/"+element, numero, "none")	
			print('</li>')
			
	print('</ul>')
	return numero


print("""content-type:text/html \n\n
<html>
	<head><meta charset="UTF-8"/>
	
	<style>
            li:hover {
		     background-color:rgba(193, 240, 234, 0.5);
		      }
	    ul{
		padding-left: 15px;
	      }
	</style>
	
	<script type="text/javascript" src="../liste.js"></script>
	
	</head>
	
	<body>""")

parcours("/auto_home/azadri/public_html", 1, "inline")

print("""
</body>
</html>""")
#parcours("/auto_home/azadri/Bureau/TEST", 1, "inline")



