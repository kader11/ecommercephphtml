#!/usr/bin/env python3
import os, re, cgi

params={}
prefixe={}



for key in cgi.FieldStorage():
	params[key]=cgi.FieldStorage()[key].value



print("""content-type:text/html \n\n
<html>
	<head><meta charset="UTF-8"/>
	<style>
		div { 
		      float:left;
		      width:75px;
		      height:100px;
		      font-family: Arial;
		      font-size: 0.8em;
		      overflow:hidden;
		      margin:5px;
		      text-align: center;
		    }
		div:hover {
		      /*background-color:rgba(193, 240, 234, 0.5);*/
		      opacity: 0.7;
			   }
		
	</style>
	</head>
	<body>""")
print("Le script est appele avec le dossier", params['rep'], "<br/><br/>")

#On utilise un dictionnaire prefixe={} dans lequel on stock les préfixes des images déja mises dans un dossier IMG
#on pourra du coup rajouter dautres icones pour d'autres extensions qui peuvent avoir lieu

image = os.listdir("/auto_home/azadri/public_html/img")

for img in image:
	
	res = re.search("(.+)\..+",img)
	if res :
		pfx = res.group(1)
	
		if pfx in prefixe :
			prefixe[pfx] += img
		else :
			prefixe[pfx] = img

#On consulte les éléments du répertoire visité et on compare les extensions des éléments aux clés de prefixe{}

contenu = os.listdir(params['rep'])
for element in contenu :
	
	if os.path.isdir(params['rep']+"/"+element):
		print("<div id='dossier' name='corps'>")
		print("<a href='contenu.cgi?rep="+params['rep']+"/"+element+"'><img src='../img/folder.png' width='50' height='50'/><br/>",element, "</a>")
		print("</div>")
	else :
		res = re.search(".+\.(.+)",element)
		if res : 			
			ext = res.group(1)

			if ext in prefixe:
				nomImg = "../img/"+prefixe[ext]
				if os.path.isfile(nomImg):
					print("<div id='fichiers' ><img src='"+nomImg+"' width='50' height='50'/><br/>",element, "</div>")
			
			#on affiche une icone standard pour les formats de fichier inconnus
			else:		
				print("<div id='deuxieme'><img src='../img/file.png' width='50' height='50'/><br/>", element, "</div>")
				


print("""</body>
</html>""")



		
