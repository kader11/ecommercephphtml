<?php

// CORRECTION TP 6 EXERCICE 4

// on prévient le client qu'on va lui envoyer du JSON
header('Content-type: application/json');

// lecture des infos existantes
$infos = file_get_contents('storage.json');

// on dit au client que tout s'est bien passé (code HTTP "ok")
http_response_code(200);

// on envoie le résultat sur la sortie; puisqu'on a lu du JSON dans le fichier on peut l'envoyer tel quel
echo $infos;

