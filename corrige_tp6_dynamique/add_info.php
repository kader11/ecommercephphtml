<?php

// CORRECTION TP 6 EXERCICE 4

// on prévient le client qu'on va lui envoyer du JSON
header('Content-type: application/json');

// lecture des infos existantes
$infos = json_decode(file_get_contents('storage.json'), true);

$ok = false;
// y a-t-il une nouvelle info postée ?
if (! empty($_REQUEST['info'])) {
	// ajout aux infos existantes et sauvegarde
	array_push($infos, [
		"info" => $_REQUEST['info'],
		"date" => date('Y-m-d H:i:s')
	]);
	$ok = file_put_contents('storage.json', json_encode($infos));
}

if ($ok === false) {
	// on dit au client que l'écriture a raté (code HTTP "internal server error")
	http_response_code(500);
} else {
	// on dit au client que tout s'est bien passé (code HTTP "created")
	http_response_code(201);
}

// pas la peine d'envoyer un résultat, le code HTTP est suffisant

