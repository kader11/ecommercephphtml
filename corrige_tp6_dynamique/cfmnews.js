// on attend que la page soit chargée
document.addEventListener('DOMContentLoaded', function() {

	// on cible une fois pour toutes le <div> dans lequel afficher les infos
	var divInfos = document.querySelector('#info-zone');

	// on prépare une fonction qui interroge le service
	function lireInfos() {
		var requete = new XMLHttpRequest();
		// on détermine ce qui doit se passer quand on recevra la réponse du service
		requete.addEventListener("load", function() {
			// on décode le format JSON pour obtenir un objet Javascript
			var infos = JSON.parse(this.responseText);
			// on boucle sur les infor reçues
			var infosStructurees = '';
			for (var i=0; i < infos.length; i++) {
				// on ajoute une info structurée
				 infosStructurees += '<div class="une-info"><label>'  + infos[i].date + '</label> ' + infos[i].info + '</div>';
			}
			// on écrit tout dans le <div>
			divInfos.innerHTML = infosStructurees;
		});
		// on envoie la requête à l'URL du webservice
		requete.open("GET", "get_infos.php");
		requete.send();
	}

	// on appelle cette fonction toutes les 5 secondes,
	// mais en fait 5 secondes c'est long alors on va plutôt faire 1 seconde :)
	setInterval(lireInfos, 1000);
	// et on l'appelle une fois au début
	lireInfos();

	// on écoute la validation du formulaire
	var formulaire = document.querySelector('#formulaire');
	formulaire.addEventListener('submit', function(e) {
		// on récupère les données du formulaire (pratique !)
		var donneesFormulaire = new FormData(formulaire);
		// on les envoie en POST au webservice d'ajout
		var requeteEnvoi = new XMLHttpRequest();
		requeteEnvoi.open("POST", "add_info.php");
		requeteEnvoi.send(donneesFormulaire);
		// on inhibe l'action par défaut du formulaire (recharger la page)
		e.preventDefault();
	});
});

