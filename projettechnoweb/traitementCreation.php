<?php
	include('entete.php');
	$donneesUsers = file_get_contents('users.json'); // Récupère le contenu du fichier users.json sous forme d'une chaine de caractères
	
	$listeUsers = json_decode($donneesUsers, true); // Converti cette chaine de caractère en une liste ($liste = false si le fichier .json est mal écrit)
	
	if($listeUsers == false){
		echo('Nous avons rencontré une erreur dans notre base de donnée lors de la création de votre compte et n\'avons pas pu vous enregister. Veuillez nous excuser et réessayez un peu plus tard.');
		header('refresh:2;url=index.php');
	}else{
		$login = $_POST['loginCreation'];
		$prenom = $_POST['prenomCreation'];
		$nom = $_POST['nomCreation'];
		$email = $_POST['emailCreation'];
		$dateNaissance = $_POST['dateCreation'];
		$motDePasse = $_POST['mdp1Creation'];
		$listeUsers[$login] = array("nom" => $nom, "prenom" => $prenom, "date_naissance" => $dateNaissance, "adresse_mail" => $email, "mot_de_passe" => $motDePasse);
		
		$donneesUsers = json_encode($listeUsers); // Transforme la liste $listeUsers en chaine de caractère au format .json et la stocke dans la variable $donneesUsers
		file_put_contents('users.json', $donneesUsers); //Enregistre cette chaine de caractere dans le fichier 'users.json'
		echo('Félicitation ! Votre compte a été crée avec succès !');
		header('refresh:2;url=index.php');
	}
	
	include('pied.html');
	
	exit; // A toujours placer apres un header pour indiquer l'arret de la lecture du script
?>
