// Ceci est le fichier javascript qui rend notre application dynamique !

document.addEventListener('DOMContentLoaded', function () {
	
	function afficheFormulaire(){
		if(document.getElementById('formulaire').style.display == 'none'){
			document.getElementById('formulaire').style.display = 'inline';
			document.getElementById('afficherFormulaire').innerHTML = 'Masquer Le Formulaire';
		}else{
			document.getElementById('formulaire').style.display = 'none';
			document.getElementById('ajoutReussi').style.display = 'none';
			document.getElementById('existeDeja').style.display = 'none';
			document.getElementById('afficherFormulaire').innerHTML = 'Veuillez déposer votre annonce ici !!';
		}	
	}
	
	function ajouterPlante(){
		// ON RECUPERE LES DONNES QUI ONT ETEES RENTREES DANS LE FORMULAIRE ::
		var titre= document.getElementById('titreannonce').value;
		var decrire le produit = document.getElementById('description').value;
		var gategorie du produit = document.getElementById('categorie').value;
		var Nom du vendeur = document.getElementById('nom_vendeur').value;
		var Prix = document.getElementById('Prix').value;		
		var rdv_lat = document.getElementById('rdv_lat').value;
		var rdv_log = document.getElementById('rdv_log').value;
		var URL de la photo= document.getElementById('urlPhoto').value;
		var date = document.getElementById('date-ajout').value;
		var Ajouter = document.getElementById('Ajouter').value;
	
		
		var request = new XMLHttpRequest();
		
		request.addEventListener('load', function(data){
			var reponse = data.target.status;
		
			if (reponse == 200){
				console.log('Votre annonce à bien était ajoutée ');
				document.getElementById('existeDeja').style.display = 'none';
				document.getElementById('ajoutReussi').style.display = 'inline';
			}else{
				if (reponse == 403){
					console.log('Votre annonce existe déja');
					document.getElementById('existeDeja').style.display = 'inline';
					document.getElementById('ajoutReussi').style.display = 'none';
				}else{
					document.getElementById('ajoutReussi').style.display = 'none';
					document.getElementById('existeDeja').style.display = 'none';
					console.log('Erreur');
				}
			}
		
		});
		
		request.open("GET", "ajout.php?= titreannonce" + titreannonce+ "&description=" + description + "&categorie =" + categorie+ "&Nom_vendeur=" + Nom_vendeur + "&Prix=" + Prix + "&rdv_lat=" + rdv_lat+ "&rdv_log=" + dv_log + "&date_ajout=" + date_ajout);
		request.send();	
	}
	
	document.getElementById('afficherFormulaire').addEventListener('click',afficheFormulaire);
	document.getElementById('ajouter').addEventListener('click', function(evt) {
		/*evt.preventDefault(); // permet de ne pas actualiser la page lorque l'on clique sur le bouton de type "submit" d'id "ajouter"*/
		ajouterPlante();
	});
	
	
});
