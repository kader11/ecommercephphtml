<?php
	include('entete.html');
?>

	<form id="contactFormulaire" method="post" action="traitementContact.php">
	   	 <fieldset>
    			<legend>Vos coordonnées</legend>
    		
	    		<p>
	    			<label>Nom : </label>
	    			<input type="text" name="nomContact" placeholder="Entrez votre nom" required />
	    		</p>
	    		
	    		<p>
	    			<label>Email : </label>
	    			<input type="email" name="mailContact" placeholder="Entrez votre email" required />
	    		</p>
	     	
	    		<p>
	    			<label>Objet : </label>
	    			<input type="text" name="objetContact" required />
	    		</p>
	    	
	    		<p>
	    			<label>Message : </label>
	    			</br>
	    			</br>
	    			<textarea name="messageContact" cols="30" rows="8"></textarea>
	    		</p>
    	    	</fieldset>

    	<div style="text-align:center;">
    		<input type="submit" name="envoyerContact" value="Envoyer votre message" />
    	</div>
    
    </form>

<?php
	include('pied.html');
?>