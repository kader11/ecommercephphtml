<?php
	include('entete.html');
?>
	<script type="text/javascript" src="devenir_membre.js" /></script>
	<form method="POST" id="formCreation" action='traitementCreation.php' onsubmit="return false">
	<!--PENSER A PEUT ETRE SUPPRIMER LES ID INUTILES --> 
		<div id='formulaireCreation'>
		<fieldset>

			<label>Prénom : </label>
			<input type="text" name="prenomCreation" placeholder="Entrez votre prénom ici ..." maxlength="20" required />
			</br>

		
			<label>Nom : </label>
			<input type="text" name="nomCreation" placeholder="Entrez votre nom ici ..." required maxlength="20"/>
			</br>
		

			<label>Date de naissance : </label>
			<input type="date" name="dateCreation" placeholder="Date jj/mm/aa" size="10" required />
			</br>
		

			<label>E-mail : </label>
			<input type="email" id="emailCreation" name="emailCreation" placeholder="votreadresse@wanadoo.fr" maxlength="75"required/>
			<div id="erreurEmail" style="color:red;">Vous avez déjà un compte avec cette adresse mail, veuillez vous connecter avec celui-ci.</div>
			</br>

			
			<label>Pseudo : </label>
			<input type="text" id="loginCreation" name="loginCreation" placeholder="Entrez votre login ici ..." maxlength="10" required/>
			<div id="erreurLogin" style="color:red;">Le pseudo que vous avez entré existe déja. Veuillez en choisir un autre.</div>
			</br>
		

			<label>Mot de passe : </label>
			<input type="password" id="mdp1Creation" name="mdp1Creation" placeholder="Entrez votre mot de passe ici ..." minlength="8" maxlength="16"required/>
			</br>
			

			<label>Confirmez votre mot de passe : </label>
			<input type="password" id="mdp2Creation" name="mdp2Creation" placeholder="Entrez votre mot de passe ici ..." minlength="8" maxlength="16" required/>
			<div id="erreurMotdepasse" style="color:red;"> Vos deux mots de passes doivent être identiques</div>
			</br>
		
				
		<label id="labelValiderCreation">Vérifier mes informations</label>
		<input id="validerCreation" type="submit" value=&#8618;>
			</div>	
		</fieldset>		
	</form>
<?php
	include('pied.html');
?>
