// Ceci est le fichier javascript qui permet de vérifier les informations entrées dans le formulaire avant de les envoyer !

document.addEventListener('DOMContentLoaded', function () { // Permet d'attendre que la page soit chargée pour exécuter le javascript, permet de placer ainsi ce script dans le <head>

	function verificationDonnees(){
		var request = new XMLHttpRequest(); // On prépare la première requête qui consistera a connaître l'état de connexion de l'utilisateur
		var login = document.getElementById('loginCreation').value; // On récupère la valeur du pseudo entrée dans le formulaire
		var email = document.getElementById('emailCreation').value; // On récupère la valeur du mail entrée dans le formulaire
		var nouveauPseudo = true;
		var nouvelEmail = true;
		
		request.addEventListener('load', function(data){ // On  définit ce que fera la fonction lorsqu'elle aura reçu une réponse de la requête
			var listeUsers = JSON.parse(data.target.responseText); // On récupère la liste des comptes déjà créés (cette liste est visible à l'adresse http://localhost/.../ProjetWEB/get_users.php
			
			for(pseudo in listeUsers){ // On va parcourir la liste de nos comptes pour vérifier que les logins et emails entrés dans le formulaire ne s'y trouvent pas déjà
				if (login == pseudo){ // Si le pseudo entré existe déjà
					nouveauPseudo = false; // Alors précise qu'il n'est pas nouveau
				}else{ //Sinon :
					if (listeUsers[pseudo]['adresse_mail'] == email){ // Si l'email entré existe déjà
						nouvelEmail = false; // Alors précise qu'il n'est pas nouveau
					}
				}
			}
			
			if (nouveauPseudo == false){ // Si le pseudo entré n'est pas nouveau
				document.getElementById('erreurLogin').style.display = 'inline'; // On affiche un message d'erreur
			}else{ // Sinon
				document.getElementById('erreurLogin').style.display = 'none'; // On désafiche le message d'erreur (au cas où il ait déjà été affiché auparavant)
				if(nouvelEmail == false){ // Si l'email entré n'est pas nouveau
					document.getElementById('erreurEmail').style.display = 'inline'; // Message d'erreur
				}else{ // Sinon 
					document.getElementById('erreurEmail').style.display = 'none'; // On désafiche le message d'erreur
					if (document.getElementById('mdp1Creation').value != document.getElementById('mdp2Creation').value){ // Si les deux mdp entrés ne sont pas égaux
						document.getElementById('erreurMdp').style.display = 'inline'; // On affiche message d'erreur
					}else{ //Sinon
						document.getElementById('labelValiderCreation').innerHTML = 'Informations valides. Créer mon compte : '; // On change la valeur du label du bouton de soumission du formulaire
						document.getElementById('validerCreation').value = 'GO' // On change la valeur du bouton de soumission
						document.getElementById('donneesFormulaireCreation').style.display = 'none'; // On désafiche le formulaire
						document.getElementById('formCreation').onsubmit="return true"; // On autorise l'envoie des données du formulaire
					}				
				}
			
			}
		});
			
		request.open("GET", "get_users.php"); // On précise que la requête sera envoyée à get_users.php
		request.send(); // On envoie la requête
	}
	
	document.getElementById('validerCreation').addEventListener('click', verificationDonnees);
});
