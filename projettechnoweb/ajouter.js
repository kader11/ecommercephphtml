// Ceci est le fichier javascript qui rend notre application dynamique !

document.addEventListener('DOMContentLoaded', function () {
	
	function afficheFormulaire(){
		if(document.getElementById('formulaire').style.display == 'none'){
			document.getElementById('formulaire').style.display = 'inline';
			document.getElementById('afficherFormulaire').innerHTML = 'Masquer Le Formulaire';
		}else{
			document.getElementById('formulaire').style.display = 'none';
			document.getElementById('ajoutReussi').style.display = 'none';
			document.getElementById('existeDeja').style.display = 'none';
			document.getElementById('afficherFormulaire').innerHTML = 'Veuillez déposer votre annonce ici !!';
		}	
	}
	
	function ajouterannonces(){
		// on recupure la structure de formulaire d'ajout d'annonce a savoir les données d'entrées  ::

		var TITRE = document.getElementById('titreannonce').value;
		var lieu = document.getElementById('decription').value;
		var latitude = document.getElementById('categorie').value;
		var longitude = document.getElementById('nom_vendeur').value;
		var date_releve = document.getElementById('pirx').value;
		var photo = document.getElementById('rdv_lat').value;
		var nom_collecteur = document.getElementById('rdv_log').value;
		var prenom_collecteur = document.getElementById('urlphoto').value;
		var commentaire = document.getElementById('date_ajout').value;
				
		var request = new XMLHttpRequest();
		
		request.addEventListener('load', function(data){
			var reponse = data.target.status;
		
			if (reponse == 200){
				console.log('Votre annonce à bien était ajoutée');
				document.getElementById('existeDeja').style.display = 'none';
				document.getElementById('ajoutReussi').style.display = 'inline';
			}else{
				if (reponse == 403){
					console.log('Annonce existe déja');
					document.getElementById('existeDeja').style.display = 'inline';
					document.getElementById('ajoutReussi').style.display = 'none';
				}else{
					document.getElementById('ajoutReussi').style.display = 'none';
					document.getElementById('existeDeja').style.display = 'none';
					console.log('Erreur');
				}
			}
		
		});
		
		request.open("GET", "ajouter.php?titreannonce=" + titreannonce + "&description=" +description + "&categorie=" + categorie + "&nom_Vendeur=" + nom_Vendeur + "&prix=" + prix + "&rdv_lat=" + rdv_lat +
 "&rdv-log=" + rdv_log + "&urlPhoto=" +urlPhoto + "&date_ajout=" + date_ajout + "&ajouter=" + ajouter);
		request.send();	
	}
	
	document.getElementById('afficherFormulaire').addEventListener('click',afficheFormulaire);
	document.getElementById('ajouter').addEventListener('click', function(evt) {
		/*evt.preventDefault(); // permet de ne pas actualiser la page lorque l'on clique sur le bouton de type "submit" d'id "ajouter"*/
		ajouterPlante();
	});
	
	
});
s